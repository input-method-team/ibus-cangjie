# Copyright (c) 2024 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from ibus_cangjie.ibus_helper import (  # type: ignore
    IBusPropList,
    IBusProperty,
)
import random
import string
import gi  # type: ignore

gi.require_version("IBus", "1.0")

from gi.repository import IBus  # type: ignore


class IBusPropListTestCase(unittest.TestCase):

    def __randString(self, l: int) -> str:
        return "".join(
            random.choices(string.ascii_lowercase + string.ascii_uppercase, k=l)
        )

    def test_ibus_prop_list__raw(self):
        pl = IBusPropList()
        self.assertIsInstance(
            pl.raw(), IBus.PropList, "IBusPropList.raw() should return IBus.PropList"
        )

    def test_ibus_prop_list__append(self):
        pl = IBusPropList()
        p = IBusProperty(
            key=self.__randString(10),
        )
        pl.append(p)

        self.assertEqual(len(pl), 1, "IBusPropList.append() should append one property")
        self.assertEqual(
            pl[p.key].key,
            p.key,
            "IBusPropList.append() should append the same property",
        )

    def test_ibus_prop_list__setitem(self):
        pl = IBusPropList()
        p = IBusProperty(
            key=self.__randString(10),
        )
        pl[p.key] = p

        self.assertEqual(
            len(pl), 1, "IBusPropList.__setitem__() should append one property"
        )
        self.assertEqual(
            pl[p.key].key,
            p.key,
            "IBusPropList.__setitem__() should append the same property",
        )

        p = IBusProperty(
            key=self.__randString(10),
        )
        with self.assertRaises(
            ValueError,
            msg="IBusPropList.__setitem__() should raise ValueError for different key in prop than in dict",
        ):
            pl[self.__randString(10)] = p

    def test_ibus_prop_list__setitem__duplicated(self):
        rand = self.__randString(10)
        plist = IBusPropList()
        p1 = IBusProperty(
            key=rand,
        )
        p2 = IBusProperty(
            key=rand,
        )
        plist.append(p1)
        with self.assertRaises(
            ValueError,
            msg="IBusPropList.__setitem__() should raise ValueError for duplicated key",
        ):
            plist.append(p2)
        with self.assertRaises(
            ValueError,
            msg="IBusPropList.__setitem__() should raise ValueError for duplicated key",
        ):
            plist[p2.key] = p2


class IBusPropertyTestCase(unittest.TestCase):
    def __randString(self, l: int) -> str:
        return "".join(
            random.choices(string.ascii_lowercase + string.ascii_uppercase, k=l)
        )

    def test_ibus_property__key(self):
        rand1 = self.__randString(10)
        p = IBusProperty(
            key=rand1,
            label="label",
        )
        self.assertEqual(p.key, rand1, "property.key not equal to assigned one")

        rand2 = self.__randString(10)
        with self.assertRaises(AttributeError, msg="property.key should be read-only"):
            p.key = rand2

    def test_ibus_property__prop_type(self):
        p = IBusProperty(
            key="key",
            label="label",
            prop_type=IBus.PropType.NORMAL,  # 0
        )
        self.assertEqual(
            p.prop_type,
            IBus.PropType.NORMAL,
            "property.prop_type not equal to assigned one",
        )

        p = IBusProperty(
            key="key",
            label="label",
            prop_type=IBus.PropType.TOGGLE,  # 1
        )
        self.assertEqual(
            p.prop_type,
            IBus.PropType.TOGGLE,
            "property.prop_type not equal to assigned one",
        )

        with self.assertRaises(
            AttributeError, msg="property.prop_type should be read-only"
        ):
            p.prop_type = IBus.PropType.NORMAL

    def test_ibus_property__label(self):
        rand1 = self.__randString(10)
        p = IBusProperty(
            key="key",
            label=rand1,
        )
        self.assertEqual(p.label, rand1, "property.label not equal to assigned one")

        rand2 = self.__randString(10)
        p.label = rand2
        self.assertEqual(p.label, rand2, "property.label not equal to reassigned one")

    def test_ibus_property__symbol(self):
        rand1 = self.__randString(10)
        p = IBusProperty(
            key="key",
            label="label",
            symbol=rand1,
        )
        self.assertEqual(p.symbol, rand1, "property.symbol not equal to assigned one")

        rand2 = self.__randString(10)
        p.symbol = rand2
        self.assertEqual(p.symbol, rand2, "property.symbol not equal to reassigned one")

    def test_ibus_property__icon(self):
        rand1 = self.__randString(10)
        p = IBusProperty(
            key="key",
            label="label",
            icon=rand1,
        )
        self.assertEqual(p.icon, rand1, "property.icon not equal to assigned one")

        rand2 = self.__randString(10)
        p.icon = rand2
        self.assertEqual(p.icon, rand2, "property.icon not equal to reassigned one")

    def test_ibus_property__tooltip(self):
        rand1 = self.__randString(10)
        p = IBusProperty(
            key="key",
            label="label",
            tooltip=rand1,
        )
        self.assertEqual(p.tooltip, rand1, "property.tooltip not equal to assigned one")

        rand2 = self.__randString(10)
        p.tooltip = rand2
        self.assertEqual(
            p.tooltip, rand2, "property.tooltip not equal to reassigned one"
        )

    def test_ibus_property__sensitive(self):
        p = IBusProperty(
            key="key",
            label="label",
            sensitive=True,
        )
        self.assertEqual(
            p.sensitive, True, "property.sensitive not equal to assigned one"
        )

        p.sensitive = False
        self.assertEqual(
            p.sensitive, False, "property.sensitive not equal to reassigned one"
        )

        p = IBusProperty(
            key="key",
            label="label",
            sensitive=False,
        )
        self.assertEqual(
            p.sensitive, False, "property.sensitive not equal to assigned one"
        )

        p.sensitive = True
        self.assertEqual(
            p.sensitive, True, "property.sensitive not equal to reassigned one"
        )

    def test_ibus_property__visible(self):
        p = IBusProperty(
            key="key",
            label="label",
            visible=True,
        )
        self.assertEqual(p.visible, True, "property.visible not equal to assigned one")

        p.visible = False
        self.assertEqual(
            p.visible, False, "property.visible not equal to reassigned one"
        )

        p = IBusProperty(
            key="key",
            label="label",
            visible=False,
        )
        self.assertEqual(p.visible, False, "property.visible not equal to assigned one")

        p.visible = True
        self.assertEqual(
            p.visible, True, "property.visible not equal to reassigned one"
        )

    def test_ibus_property__state(self):
        p = IBusProperty(
            key="key",
            label="label",
            state=IBus.PropState.CHECKED,
        )
        self.assertEqual(
            p.state, IBus.PropState.CHECKED, "property.state not equal to assigned one"
        )

        p.state = IBus.PropState.UNCHECKED
        self.assertEqual(
            p.state,
            IBus.PropState.UNCHECKED,
            "property.state not equal to reassigned one",
        )

        p = IBusProperty(
            key="key",
            label="label",
            state=IBus.PropState.UNCHECKED,
        )
        self.assertEqual(
            p.state,
            IBus.PropState.UNCHECKED,
            "property.state not equal to assigned one",
        )

        p.state = IBus.PropState.CHECKED
        self.assertEqual(
            p.state,
            IBus.PropState.CHECKED,
            "property.state not equal to reassigned one",
        )

    def test_ibus_property__undef(self):
        with self.assertRaises(
            TypeError, msg="property.undef should raise TypeError for undefined keyword"
        ):
            p = IBusProperty(
                key="key",
                label="label",
                undef="abc",
            )

        p = IBusProperty(
            key="key",
            label="label",
        )
        with self.assertRaises(
            AttributeError, msg="property.undef should raise AttributeError"
        ):
            p.undef = "abc"
