# Copyright (c) 2013 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.


import math
import unittest

from gi.repository import IBus  # type: ignore

from ibus_cangjie.engine import *  # type: ignore


class QuickTestCase(unittest.TestCase):
    def setUp(self):
        self.engine = EngineQuick()

    def tearDown(self):
        del self.engine

    def test_single_key(self):
        self.engine.do_process_key_event(IBus.a, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 1)

    def test_single_key_space_single_char(self):
        self.engine.do_process_key_event(IBus.d, 0, 0)
        self.engine.do_process_key_event(IBus.space, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 1)

    def test_single_key_space_two_candidates(self):
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.space, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 1)
        self.assertEqual(len(self.engine._mock_committed_text), 0)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 2)

    def test_two_candidates_space(self):
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.space, 0, 0)

        # Keep track of the first candidate, check later if it was committed
        expected = self.engine.lookuptable.get_candidate(0).text

        self.engine.do_process_key_event(IBus.space, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 1)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)
        self.assertEqual(self.engine._mock_committed_text, expected)

    def test_two_candidates_continue_input(self):
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.space, 0, 0)

        # Keep track of the first candidate, check later if it was committed
        expected = self.engine.lookuptable.get_candidate(0).text

        self.engine.do_process_key_event(IBus.a, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 1)
        self.assertEqual(len(self.engine._mock_committed_text), 1)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)
        self.assertEqual(self.engine._mock_committed_text, expected)

    def test_auto_candidates(self):
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.a, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 2)
        self.assertEqual(len(self.engine._mock_committed_text), 0)
        self.assertTrue(self.engine.lookuptable.get_number_of_candidates() > 1)

    def test_inexistent_combination(self):
        self.engine.do_process_key_event(IBus.x, 0, 0)
        self.engine.do_process_key_event(IBus.z, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 2)
        self.assertEqual(len(self.engine._mock_committed_text), 0)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)
        self.assertEqual(len(self.engine.sounds._mock_played_events), 1)

    def test_nowildcard(self):
        self.engine.do_process_key_event(IBus.d, 0, 0)
        self.engine.do_process_key_event(IBus.asterisk, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 2)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

        self.engine.do_process_key_event(IBus.d, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 1)
        self.assertEqual(len(self.engine._mock_committed_text), 2)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

    def test_nowildcard_first(self):
        self.engine.do_process_key_event(IBus.asterisk, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 1)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

    def test_backspace(self):
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.BackSpace, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 0)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

    def test_backspace_on_multiple_keys(self):
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.BackSpace, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 1)
        self.assertEqual(len(self.engine._mock_committed_text), 0)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

    def test_backspace_on_candidates(self):
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.space, 0, 0)
        self.engine.do_process_key_event(IBus.BackSpace, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 0)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

    def test_escape(self):
        self.engine.do_process_key_event(IBus.d, 0, 0)
        self.engine.do_process_key_event(IBus.d, 0, 0)

        self.engine.do_process_key_event(IBus.Escape, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 0)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

    def test_symbol(self):
        self.engine.do_process_key_event(IBus.at, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 1)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

    def test_multiple_punctuation(self):
        self.engine.do_process_key_event(IBus.comma, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 1)
        self.assertEqual(len(self.engine._mock_committed_text), 0)
        self.assertTrue(self.engine.lookuptable.get_number_of_candidates() > 1)

    def test_char_then_multiple_punctuation(self):
        self.engine.do_process_key_event(IBus.d, 0, 0)
        self.engine.do_process_key_event(IBus.comma, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 1)
        self.assertEqual(len(self.engine._mock_committed_text), 1)
        self.assertTrue(self.engine.lookuptable.get_number_of_candidates() > 1)

    def test_punctuation_then_punctuation(self):
        self.engine.do_process_key_event(IBus.comma, 0, 0)
        self.engine.do_process_key_event(IBus.comma, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 1)
        self.assertEqual(len(self.engine._mock_committed_text), 1)
        self.assertTrue(self.engine.lookuptable.get_number_of_candidates() > 1)

    def test_commit_with_numpad(self):
        self.engine.do_process_key_event(IBus.h, 0, 0)
        self.engine.do_process_key_event(IBus.i, 0, 0)
        self.engine.do_process_key_event(getattr(IBus, "7"), 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 1)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

        # Reset the committed text, but keep the value first
        expected = self.engine._mock_committed_text[:]
        self.engine._mock_committed_text = ""

        self.engine.do_process_key_event(IBus.h, 0, 0)
        self.engine.do_process_key_event(IBus.i, 0, 0)
        self.engine.do_process_key_event(IBus.KP_7, 0, 0)

        self.assertEqual(len(self.engine._mock_auxiliary_text), 0)
        self.assertEqual(len(self.engine._mock_committed_text), 1)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 0)

        # Now check that the same character was committed
        self.assertEqual(expected, self.engine._mock_committed_text)

    def test_modified_key_events(self):
        self.assertFalse(
            self.engine.do_process_key_event(IBus.a, 0, IBus.ModifierType.CONTROL_MASK),
            "Should not have been processed Ctrl+a",
        )
        self.assertFalse(
            self.engine.do_process_key_event(IBus.a, 0, IBus.ModifierType.MOD1_MASK),
            "Should not have been processed Alt+a",
        )
        self.assertFalse(
            self.engine.do_process_key_event(IBus.a, 0, IBus.ModifierType.MOD4_MASK),
            "Should not have been processed Super_L+a",
        )
        self.assertFalse(
            self.engine.do_process_key_event(IBus.a, 0, IBus.ModifierType.MOD4_MASK),
            "Should not have been processed Mode_switch+a",
        )

    def test_lookup_with_zero(self):
        # Type "ab" that prodcues 14 candidates
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.b, 0, 0)
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 14)

        # Key in "0" should return True because it is handled
        self.assertTrue(
            self.engine.do_process_key_event(IBus.KP_0, 0, 0),
            "Pressing '0' in candidate selection should have been handled",
        )
        self.assertGreater(
            self.engine.lookuptable.get_number_of_candidates(),
            0,
            "Pressing '0' should not clear the lookuptable",
        )
        self.assertEqual(
            len(self.engine._mock_committed_text),
            0,
            "Pressing '0' should not have committed any text",
        )

    def test_lookup_with_out_of_bound_index(self):
        # Type "ab" that prodcues 14 candidates, 2 pages by default
        # with the second page of only 5 candidates.
        #
        # So pressing page down to the next page and then pressing '6'
        # will be out of bound.
        self.engine.do_process_key_event(IBus.a, 0, 0)
        self.engine.do_process_key_event(IBus.b, 0, 0)

        # Checks the number of candidates to see if there is any systematic
        # changes.
        self.assertEqual(self.engine.lookuptable.get_number_of_candidates(), 14)

        # Calculate the out of bound index
        num_candidates = self.engine.lookuptable.get_number_of_candidates()
        page_size = self.engine.lookuptable.get_page_size()
        num_pages = math.ceil(num_candidates / page_size)
        remainder = num_candidates % page_size
        num_last_candidate = remainder if remainder > 0 else page_size
        num_out_of_bound = num_last_candidate + 1

        if num_out_of_bound > 9:
            # Test won't make sense if the out of bound index is greater than 9.
            # This is for the fringe case where the system default changes and
            # the last page of the selected input radicals has exactly 9
            # candidates.
            #
            # In which case, please rewrite the test case with other input
            # radicals.
            self.fail(
                f"Test case is invalid, the out of bound index '{num_out_of_bound}' is greater than 9"
            )
            return

        # Press Page_Down until the last page
        for _ in range(num_pages - 1):
            self.engine.do_process_key_event(IBus.Page_Down, 0, 0)

        # The number of item should not be divisible by page size (i.e.
        # reminder > 0) to allow the last page to have an out-of-bound num-key.
        self.assertGreaterEqual(
            remainder,
            1,
            "The last page should have at least 1 candidate",
        )

        # Press the number key of the out of bound index
        keyval = getattr(IBus, f"KP_{num_out_of_bound}")
        self.assertTrue(
            self.engine.do_process_key_event(keyval, 0, 0),
            f"Pressing '{num_out_of_bound}' in candidate selection should have been handled",
        )
        self.assertGreater(
            self.engine.lookuptable.get_number_of_candidates(),
            0,
            f"Pressing '{num_out_of_bound}' should not clear the lookuptable",
        )
        self.assertEqual(
            len(self.engine._mock_committed_text),
            0,
            f"Pressing '{num_out_of_bound}' should not have committed any text",
        )
