# Copyright (c) 2012-2013 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.


from gettext import dgettext
from typing import Dict, Tuple

import gi  # type: ignore

gi.require_version("Adw", "1")
gi.require_version("Gdk", "4.0")
gi.require_version("Gio", "2.0")
gi.require_version("GLib", "2.0")
gi.require_version("Gtk", "4.0")

from gi.repository import (  # type: ignore
    Adw,
    Gdk,
    Gio,
    GLib,
    Gtk,
)


class Setup(Adw.Application):

    __engine: str
    __datadir: str
    __text_domain: str
    __version: str
    __widgets: Dict[str, Gtk.Widget]

    __dropdown_values: Dict[str, Tuple[int, ...]] = {
        "version": (3, 5),
    }

    def __init__(self, engine, datadir, text_domain, version, **kwargs):
        super().__init__(**kwargs)
        self.__engine = engine
        self.__datadir = datadir
        self.__text_domain = text_domain
        self.__version = version
        self.__widgets = {}
        self.connect("activate", self.on_activate)

    def on_activate(self, app):
        self.settings = Gio.Settings(
            "org.freedesktop.cangjie.ibus.%s" % self.__engine.capitalize()
        )
        self.settings.connect("changed", self.on_value_changed)

        ui_file = GLib.build_filenamev([self.__datadir, "setup.ui"])
        css_file = GLib.build_filenamev([self.__datadir, "setup.css"])
        self.__builder = Gtk.Builder()
        self.__builder.set_translation_domain(self.__text_domain)
        self.__builder.add_from_file(ui_file)

        # Define button actions
        self.__builder.get_object("close-button").connect("clicked", self.__do_close)
        self.__builder.get_object("about-button").connect("clicked", self.__show_about)

        # Find all GtkDropDown widgets and define behaviour
        for key in ("version",):
            widget = self.__builder.get_object(key)
            active = self.__dropdown_values[key].index(self.settings.get_int(key))
            widget.set_selected(active)
            widget.connect("notify::selected-item", self.__on_dropdown_changed, key)
            self.__widgets[key] = widget

        # Find all GtkCheckButton widgets and define behaviour
        for key in (
            "include-allzh",
            "include-jp",
            "include-zhuyin",
            "include-symbols",
            "input-mode-hotkey-shift",
            "star-helps-learners",
        ):
            widget = self.__builder.get_object(key)
            widget.set_active(self.settings.get_boolean(key))
            widget.connect("notify::active", self.__on_switch_toggled, key)
            self.__widgets[key] = widget

        self.__window = self.__builder.get_object("setup_dialog")

        # Set the title of the window
        if self.__engine == "cangjie":
            title = dgettext(self.__text_domain, "Cangjie Preferences")
        elif self.__engine == "quick":
            title = dgettext(self.__text_domain, "Quick Preferences")

        self.__window.set_title(title)

        # Set the help information
        dialog = self.__builder.get_object("about_dialog")
        dialog.set_version(self.__version)
        dialog.set_application_icon(
            f"org.freedesktop.cangjie.ibus.{self.__engine.capitalize()}"
        )
        dialog.set_license_type(Gtk.License(Gtk.License.GPL_3_0))

        try:
            # Pretend to be a GNOME Control Center dialog if appropriate
            self.gnome_cc_xid = int(GLib.getenv("GNOME_CONTROL_CENTER_XID"))
            self.__window.set_wmclass("gnome-control-center", "Gnome-control-center")
            self.__window.set_modal(True)
            self.__window.connect("notify::window", self.set_transient)
            self.__window.set_type_hint(Gdk.WindowTypeHint.DIALOG)

        except:
            # No problem here, we're just a normal dialog
            pass

        # Load the CSS file
        css_provider = Gtk.CssProvider()
        css_provider.load_from_path(css_file)
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )

        # Presetn the window
        self.__window.set_application(self)
        self.__window.present()

    def set_transient(self, obj, pspec):
        from gi.repository import GdkX11

        window = self.__window.get_window()
        if window is not None:
            parent = GdkX11.X11Window.foreign_new_for_display(
                Gdk.Display.get_default(), self.gnome_cc_xid
            )

        if parent is not None:
            window.set_transient_for(parent)

    def on_value_changed(self, settings, key):
        """Callback when the value of a widget is changed.

        We need to react, in case the value was changed from somewhere else,
        for example from another setup UI.
        """

        if key not in self.__widgets:
            # Ignore settings change to keys we don't know about
            # (e.g. "halfwidth-chars")
            return

        widget = self.__widgets[key]
        if isinstance(widget, Gtk.DropDown):
            new_value = self.settings.get_int(key)
            active = self.__dropdown_values[key].index(new_value)
            widget.set_selected(active)

        if isinstance(widget, Gtk.CheckButton):
            new_value = self.settings.get_boolean(key)
            widget.set_active(new_value)

    def __on_switch_toggled(self, widget, active, key):
        self.settings.set_boolean(key, widget.get_active())

    def __on_dropdown_changed(self, widget, data, key):
        value = self.__dropdown_values[key][widget.get_selected()]
        self.settings.set_int(key, value)

    def __do_close(self, widget):
        self.__builder.get_object("about_dialog").close()
        self.__window.close()

    def __show_about(self, widget):
        self.__builder.get_object("about_dialog").present(self.__window)
