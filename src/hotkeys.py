# Copyright (c) 2024 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.

from enum import Enum
from threading import Lock
from typing import Any, Callable, List, Self, Tuple, Type, TypeAlias, Union

import gi  # type: ignore

gi.require_version("IBus", "1.0")
from gi.repository import IBus  # type: ignore

Key: TypeAlias = Union[int, Type["SpecialKeys"]]

KeyCombination: TypeAlias = List[Key]

KeyCombinationHandler: TypeAlias = Callable[[KeyCombination], None]

KeyCombinationAndHandler: TypeAlias = Tuple[
    KeyCombination, Callable[[KeyCombination], None]
]


class SpecialKeys(Enum):
    SHIFT_L = 42
    SHIFT_R = 54
    CTRL_L = 29
    CTRL_R = 97
    ALT_L = 56
    ALT_R = 100

    def __repr__(self):
        return self.name

    def __lt__(self, other: KeyCombination):
        if isinstance(other, SpecialKeys):
            return self.value < other.value
        if isinstance(other, int):
            return self.value < other
        raise TypeError(
            "'{op}' not supported between instances of '{self}' and '{other}'".format(
                op="<", self=type(self).__name__, other=type(other).__name__
            )
        )

    def __gt__(self, other: KeyCombination):
        if isinstance(other, SpecialKeys):
            return self.value > other.value
        if isinstance(other, int):
            return self.value > other
        raise TypeError(
            "'{op}' not supported between instances of '{self}' and '{other}'".format(
                op=">", self=type(self).__name__, other=type(other).__name__
            )
        )

    def __eq__(self, other: Any):
        if isinstance(other, SpecialKeys):
            return self.value == other.value
        if isinstance(other, int):
            return self.value == other
        raise TypeError(
            "'{op}' not supported between instances of '{self}' and '{other}'".format(
                op="==", self=type(self).__name__, other=type(other).__name__
            )
        )


class HotKeyManager:
    # A list of activated special keys
    __activated: List[KeyCombination] = []

    # A list of special keys that are previously activated
    # and released without triggering clear
    __live: List[KeyCombination] = []

    # A list of hotkey callbacks
    __hotkey_callback: List[KeyCombinationAndHandler] = []

    # A lock to prevent race condition when using in threads
    __lock: Lock

    def __init__(self):
        # A list of activated special keys
        self.__activated = []

        # A list of special keys that are previously activated
        # and released without triggering clear
        self.__live = []

        # A list of hotkey callbacks
        self.__hotkey_callback = []

        self.__lock = Lock()

    def register_hotkey(
        self, key_code_combination: KeyCombination, callback: KeyCombinationHandler
    ) -> Self:
        """Register a set of hotkey with a callback

        Args:
            key_code_combination: A list of key codes
            callback: A callback function to be triggered when the key
                      combination is pressed
        """
        if not isinstance(key_code_combination, list):
            key_code_combination = [key_code_combination]
        else:
            key_code_combination.sort()

        if (key_code_combination, callback) not in self.__hotkey_callback:
            self.__hotkey_callback.append((key_code_combination, callback))
        return self

    def deregister_hotkey(
        self, key_code_combination: KeyCombination, callback: KeyCombinationHandler
    ) -> Self:
        """Deregister a set of hotkey with a callback

        Args:
            key_code_combination: A list of key codes
            callback: A callback function to be triggered when the key
                      combination is pressed
        """
        if not isinstance(key_code_combination, list):
            key_code_combination = [key_code_combination]
        else:
            key_code_combination.sort()

        if (key_code_combination, callback) in self.__hotkey_callback:
            self.__hotkey_callback.remove((key_code_combination, callback))
        return self

    def do_process_key_event(self, keyval: int, keycode: int, state: int) -> None:
        """A proxy function to hotkey-related handle key events

        Args:
            keyval: The key value
            keycode: The key code
            state: The state of the key
        """
        # Triggers method depends on keycode and state.
        # About the states, see:
        # https://valadoc.org/ibus-1.0/IBus.ModifierType.html
        if state & IBus.ModifierType.RELEASE_MASK:
            self.__do_process_key_up(keyval, keycode, state)
        else:
            self.__do_process_key_down(keyval, keycode, state)

    def __do_process_key_down(self, keyval: int, keycode: int, state: int) -> None:
        """Handle key down events

        When a key is pressed down, check if it is a special key.
        If it is, activate it. If it is already activated, do nothing.

        Args:
            keyval: The key value
            keycode: The key code
            state: The state of the key
        """
        if not self.__is_special_key(keycode):
            # Note: Can handle non-special keys with
            #       special key combinations
            #       (e.g. Shift+key, Ctrl+key, Alt+key)
            self.__clear()
            return
        if not self.__is_activated(keycode):
            self.__activate(keycode)

    def __do_process_key_up(self, keyval: int, keycode: int, state: int) -> None:
        """Handle key up events

        When a key is released, check if it is a special key and if
        it is activated. If it is both, convert it to live state.

        If this is the last activated key released, trigger the live hotkeys.

        Args:
            keyval: The key value
            keycode: The key code
            state: The state of the key
        """
        if not self.__is_special_key(keycode):
            return
        if not self.__is_activated(keycode):
            return
        self.__activate_to_live(keycode)
        if len(self.__activated) == 0:
            self.__trigger_live_hotkeys()
            self.__clear()

    def __trigger_live_hotkeys(self) -> None:
        """Fire the live hotkeys

        Trigger the callback for the live hotkeys if it matches the
        key combination specified
        """
        self.__lock.acquire()
        self.__live = sorted_live = sorted(self.__live)
        self.__lock.release()
        for key_code_combination, callback in self.__hotkey_callback:
            if sorted_live == key_code_combination:
                callback(key_code_combination)
                return

    def __is_special_key(self, key_code) -> bool:
        """Check if the key code belongs to a special key"""
        return key_code in SpecialKeys

    def __activate(self, keycode) -> None:
        """Activate a key code"""
        self.__activated.append(keycode)

    def __activate_to_live(self, keycode) -> None:
        """Convert the key code to live state."""
        self.__lock.acquire()
        self.__activated.remove(keycode)
        self.__live.append(keycode)
        self.__lock.release()

    def __is_activated(self, keycode) -> bool:
        """Check if the key code belongs to a previously activated key"""
        return keycode in self.__activated

    def __clear(self) -> None:
        """Clear the activated and live key codes"""
        self.__lock.acquire()
        self.__activated.clear()
        self.__live.clear()
        self.__lock.release()
