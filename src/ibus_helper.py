# Copyright (c) 2012-2013 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations
from typing import Any, Union
import gi  # type: ignore

gi.require_version("IBus", "1.0")

from gi.repository import IBus  # type: ignore

__all__ = ["IBusPropList", "IBusProperty"]


class IBusPropList(dict):
    """An enhanced version of IBus.PropList.

    Make it easier for random access to specific property after
    creation and registration.
    """

    # Property list to be registered by Engine.register_properties
    __list: IBus.PropList

    # Property dictionary for accessing properties after creation
    __dict: dict[str, IBus.Property]

    def __init__(self) -> None:
        self.__list = IBus.PropList()
        self.__dict = {}
        pass

    def raw(self) -> IBus.PropList:
        """Return the IBus.PropList"""
        return self.__list

    def append(self, prop: IBusProperty) -> None:
        """Append a property to the list"""
        key = prop.key
        if key in self.__dict:
            raise ValueError("Property already exists", key, self.__dict[key])
        self.__list.append(prop.raw())
        self.__dict[prop.key] = prop

    def __setitem__(self, key, prop: IBusProperty) -> None:
        """Implements dict magic method __setitem__"""
        if key != prop.key:
            raise ValueError("Key does not match", key, prop.key)
        self.append(prop)

    def __getitem__(self, key) -> IBusProperty:
        """Implements dict magic method __getitem__"""
        return self.__dict[key]

    def __len__(self) -> int:
        """Implements dict magic method __len__"""
        return len(self.__dict)

    def __delitem__(self, key) -> None:
        """Implements dict magic method __delitem__"""
        raise Exception("Props do not support delete operation")

    def __repr__(self) -> str:
        """Implements magic method __repr__"""
        return repr(self.__dict)


class IBusProperty(object):
    """A proxy class for IBus.Property.

    To allow using IBusPropertyDict in place of IBus.PropList
    for sub props. Easier to access subprops after creation.

    Also allow to set properties in Python native types.
    """

    __prop: IBus.Property
    __sub_props: IBusPropList

    def __init__(
        self,
        key: str = "",
        prop_type: IBus.PropType = IBus.PropType.NORMAL,
        label: str = "",
        symbol: str = "",
        icon: str = "",
        tooltip: str = "",
        state: IBus.PropState = IBus.PropState.UNCHECKED,
        sensitive: bool = True,
        visible: bool = True,
        sub_props: Union[IBusPropList, None] = None,
    ) -> None:

        self.__prop = IBus.Property(
            key=key,
            prop_type=prop_type,
            label=IBus.Text.new_from_string(label),
            symbol=IBus.Text.new_from_string(symbol),
            icon=icon,
            tooltip=IBus.Text.new_from_string(tooltip),
            state=state,
            sensitive=bool(sensitive),
            visible=bool(visible),
            sub_props=sub_props.raw() if sub_props is not None else None,
        )
        if sub_props is not None:
            self.__sub_props = sub_props
        pass

    def raw(self) -> IBus.Property:
        """Return the raw IBus.Property"""
        return self.__prop

    def __getattr__(self, name: str) -> Any:
        """Implements magic method __getattr__"""
        match name:
            case "key":
                if self.__prop is not None:
                    return self.__prop.get_key()
                return None
            case "prop_type":
                return self.__prop.get_prop_type()
            case "label":
                return self.__prop.get_label().get_text()
            case "symbol":
                return self.__prop.get_symbol().get_text()
            case "icon":
                return self.__prop.get_icon()
            case "tooltip":
                return self.__prop.get_tooltip().get_text()
            case "sensitive":
                return self.__prop.get_sensitive()
            case "visible":
                return self.__prop.get_visible()
            case "state":
                return self.__prop.get_state()
            case "sub_props":
                return self.__sub_props
        pass

    def __setattr__(self, name: str, value: Any) -> None:
        """Implements magic method __setattr__"""
        match name:
            case "key":
                raise AttributeError("Cannot modify the value of key")
            case "label":
                self.__prop.set_label(IBus.Text.new_from_string(value))
            case "symbol":
                self.__prop.set_symbol(IBus.Text.new_from_string(value))
            case "icon":
                self.__prop.set_icon(value)
            case "tooltip":
                self.__prop.set_tooltip(IBus.Text.new_from_string(value))
            case "sensitive":
                self.__prop.set_sensitive(bool(value))
            case "visible":
                self.__prop.set_visible(bool(value))
            case "state":
                self.__prop.set_state(value)
            case "sub_props":
                raise AttributeError("sub_props is read-only")
            case "_IBusProperty__prop":
                object.__setattr__(self, name, value)
            case "_IBusProperty__sub_props":
                object.__setattr__(self, name, value)
            case _:
                raise AttributeError(f"IBusProperty object has no attribute {name}")
        pass
